package factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.korzh.chatdemo.ui.chat.ChatViewModel
import repo.Repository

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

class ViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChatViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ChatViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}