package com.korzh.chatdemo

import android.app.Application
import dao.ChatRoomDatabase
import factory.ViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import repo.DataBase
import repo.Repository

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

class ChatApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())
    private val room by lazy { ChatRoomDatabase.getDatabase(this, applicationScope) }
    private val database by lazy { DataBase(room.messageDao()) }
    private val repository by lazy { Repository(database) }
    val factory by lazy { ViewModelFactory(repository) }
}