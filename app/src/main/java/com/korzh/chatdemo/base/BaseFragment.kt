package com.korzh.chatdemo.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.korzh.chatdemo.ChatApplication

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

abstract class BaseFragment<ViewModelType> : Fragment() {

    protected var viewModel: ViewModelType? = null

    @LayoutRes
    abstract fun getLayoutResource(): Int
    abstract fun getViewModelClass(): Class<out ViewModel>
    abstract fun onViewModelCreated(viewModel : ViewModelType)
    abstract fun initView(view : View)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = (activity?.application as ChatApplication)
            .factory.create(getViewModelClass()) as ViewModelType
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(getLayoutResource(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel?.let { onViewModelCreated(it) }
    }
}