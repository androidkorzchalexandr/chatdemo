package com.korzh.chatdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.korzh.chatdemo.ui.chat.ChatFragment

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ChatFragment.newInstance())
                .commitNow()
        }
    }
}