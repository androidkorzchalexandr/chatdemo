package com.korzh.chatdemo.ui.chat

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.korzh.chatdemo.R
import com.korzh.chatdemo.base.BaseFragment
import com.korzh.chatdemo.ui.chat.adapter.MessagesAdapter
import dao.Message

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */
class ChatFragment : BaseFragment<ChatViewModel>() {

    private lateinit var button: Button
    private lateinit var adapter: MessagesAdapter
    private lateinit var recyclerView: RecyclerView

    companion object {
        fun newInstance() = ChatFragment()
    }

    override fun getViewModelClass() = ChatViewModel::class.java
    override fun getLayoutResource() = R.layout.chat_fragment

    override fun initView(view: View) {
        recyclerView = view.findViewById(R.id.recyclerview)
        adapter = MessagesAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        //todo remove
        button = view.findViewById<Button>(R.id.button)
    }

    override fun onViewModelCreated(viewModel: ChatViewModel) = viewModel.run {
        allMessages.observe(viewLifecycleOwner, { messages ->
            if (messages.isNotEmpty()) {
                adapter.submitList(messages)
                recyclerView.scrollToPosition(messages.lastIndex)
            }
            lastMessage.observe(viewLifecycleOwner, { message ->
                message?.let {
                    adapter.addItem(message)
                    recyclerView.scrollToPosition(adapter.itemCount.dec())
                }
            })
        })
        getMessageHistory()


        button.setOnClickListener {
            val message = Message("Text")
            viewModel.insert(message)
        }


//        repeat(300, action = {ind ->

//        })
    }
}