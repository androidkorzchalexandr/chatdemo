package com.korzh.chatdemo.ui.chat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.korzh.chatdemo.R
import dao.Message

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

class MessagesAdapter : RecyclerView.Adapter<MessagesAdapter.MessageViewHolder>() {

    private val data: MutableList<Message> = mutableListOf()

    fun submitList(messages: List<Message>) {
        data.addAll(messages)
        notifyDataSetChanged()
    }

    fun addItem(message: Message) = data.run {
        if(!get(lastIndex).equals(message)){
            add(message)
            notifyItemInserted(lastIndex)
        }
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val current = data.get(position)
        holder.bind(current.id.toString())
    }

    class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val wordItemView: TextView = itemView.findViewById(R.id.textView)

        fun bind(text: String?) {
            wordItemView.text = text
        }

        companion object {
            fun create(parent: ViewGroup): MessageViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return MessageViewHolder(view)
            }
        }
    }
}