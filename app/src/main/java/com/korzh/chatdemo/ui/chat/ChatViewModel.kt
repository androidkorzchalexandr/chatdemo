package com.korzh.chatdemo.ui.chat

import androidx.lifecycle.*
import dao.Message
import kotlinx.coroutines.launch
import repo.Repository

class ChatViewModel(val repository: Repository) : ViewModel() {

    val allMessages: MutableLiveData<List<Message>> = MutableLiveData()

    val lastMessage: LiveData<Message> = repository.getLastMessage().asLiveData()

    fun getMessageHistory( ) =  viewModelScope.launch {
        allMessages.value = repository.getMessagesHistory()
    }

    fun insert(message: Message) = viewModelScope.launch {
        repository.insertMessage(message)
    }
}