package repo

import dao.Message
import kotlinx.coroutines.flow.Flow

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

class Repository(val database: DataBase) {

    suspend fun getMessagesHistory(): List<Message> = run {
        database.getMessagesHistory()
    }

    fun getLastMessage(): Flow<Message> = run {
        database.lastMessage
    }

    suspend fun insertMessage(message: Message) {
        database.insert(message)
    }
}