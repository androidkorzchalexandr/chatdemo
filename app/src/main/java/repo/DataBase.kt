package repo

import androidx.annotation.WorkerThread
import dao.Message
import dao.MessageDao
import kotlinx.coroutines.flow.Flow

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

class DataBase(val messageDao: MessageDao) {

    suspend fun getMessagesHistory(): List<Message> = run {
        messageDao.getMessagesHistory()
    }

    val lastMessage: Flow<Message> = messageDao.getLastMessage()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(message: Message) {
        messageDao.insert(message)
    }
}