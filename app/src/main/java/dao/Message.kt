package dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

@Entity(tableName = "messages")
data class Message(
    var text: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null
    @ColumnInfo(name = "author_id")
    val authorId: Long? = null
    @ColumnInfo(name = "time")
    val time: Long? = null
    @ColumnInfo(name = "image")
    val image: String? = null
    @ColumnInfo(name = "file")
    val file: String? = null

    override fun equals(other: Any?): Boolean {
        return this.id == (other as? Message)?.id
    }

    override fun hashCode(): Int {
        var result = text?.hashCode() ?: 0
        result = 31 * result + (id ?: 0)
        result = 31 * result + (authorId?.hashCode() ?: 0)
        result = 31 * result + (time?.hashCode() ?: 0)
        result = 31 * result + (image?.hashCode() ?: 0)
        result = 31 * result + (file?.hashCode() ?: 0)
        return result
    }
}