package dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

@Dao
interface MessageDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(message: Message)

//    @Query("SELECT * FROM messages ORDER BY time ASC")
    @Query("SELECT * FROM messages")
    suspend fun getMessagesHistory(): List<Message>

    @Query("SELECT * FROM messages WHERE id=(SELECT max(id) FROM messages)")
    fun getLastMessage(): Flow<Message>

//    @Query("DELETE FROM messages")
//    /*suspend */fun deleteAll()
}