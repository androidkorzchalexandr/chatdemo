package dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * @author Alexander Korzh (alexander.korzh@dataart.com -- DataArt).
 */

@Database(entities = [Message::class], version = 1, exportSchema = false)
abstract class ChatRoomDatabase : RoomDatabase() {

    abstract fun messageDao(): MessageDao

    companion object {
        @Volatile
        private var INSTANCE: ChatRoomDatabase? = null

        private val dataBaseName = "chat_database"

        fun getDatabase(context: Context, scope: CoroutineScope): ChatRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ChatRoomDatabase::class.java,
                    dataBaseName
                ).addCallback(ChatDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }

    private class ChatDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.messageDao())
                }
            }
        }

        suspend fun populateDatabase(messageDao: MessageDao) {
            //for start working with database
        }
    }
}